package com.easyapp.lib.callback;

/**
 * 簡單的callback 回傳
 */
public class Callback<T> implements iCallback<T> {

    @Override
    public void initCallback() {

    }

    @Override
    public void callback(T object) {

    }

    @Override
    public void onFail(T object) {

    }

    @Override
    public void onFail() {

    }

    @Override
    public void onComplete() {

    }
}
